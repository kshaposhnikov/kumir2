<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>CoreGUI::AboutDialog</name>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="14"/>
        <source>About Kumir</source>
        <translation>О системе &quot;Кумир&quot;</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="24"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="34"/>
        <source>Licensee</source>
        <translation>Права на использование</translation>
    </message>
    <message utf8="true">
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="40"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Droid Sans&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-style:italic;&quot;&gt;За пределами Российской Федерации данная программа может свободно распространяться по лиценции GNU GPL v2.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-style:italic;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-style:italic;&quot;&gt;На территории Российской Федерации данная программа распространяется в соответствии с данным Лицензионным соглашением.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;ЛИЦЕНЗИОННОЕ СОГЛАШЕНИЕ&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;1. Устанавливая данный программный продукт, Вы автоматически принимаете условия данного лицензионного соглашения.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;2. Вы можете устанавливать данную программу на любое число компьютеров неограниченное число раз.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;3. Вы можете делать  неограниченное число копий данного программного продукта.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;4. Вы можете передавать копии данного программного продукта возмездно или безвозмездно неограниченному числу третьих лиц.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;5. Вы имеете право загрузить с сайта разработчика исходные тексты данного программного продукта и использовать их в соответствии с требованиями лицензии GNU GPL v2, текст которой на английском языке прилагается в файле license.gpl.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;6. Разработчик не дает никаких гарантий работоспобосности данного продукта и не несет ответственности за любой ущерб, причиненный вследствии установки или запуска данного программного продукта.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="89"/>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="109"/>
        <source>unknown</source>
        <translation>неизвестно</translation>
    </message>
    <message>
        <source>GIT hash:</source>
        <translation type="obsolete">GIT отпечаток:</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="99"/>
        <source>Last modified:</source>
        <translation>Последнее изменение:</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="146"/>
        <source>Copy system information to clipboard</source>
        <translation>Скопировать информацию о системе в буфер обмена</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="166"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="169"/>
        <source>Esc</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="79"/>
        <source>Version:</source>
        <translation>Версия:</translation>
    </message>
    <message>
        <source>Kumir version: %1 (revision: %2)</source>
        <translation type="obsolete">Версия %1 (SVN-ревизия №%2)</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="126"/>
        <source>Parameter</source>
        <translation>Параметр</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="70"/>
        <source>System Information</source>
        <translation>Информация об окружении</translation>
    </message>
    <message>
        <source>Qt Version:</source>
        <translation type="obsolete">Версия Qt</translation>
    </message>
    <message>
        <source>Operating system:</source>
        <translation type="obsolete">Операционная система</translation>
    </message>
    <message>
        <source>Launched binary:</source>
        <translation type="obsolete">Исполняемый файл:</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="131"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.cpp" line="66"/>
        <source>Copied to clipboard</source>
        <translation>Скопировано в буфер обмена</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.cpp" line="67"/>
        <source>&lt;b&gt;The following text has been copied to clipboard:&lt;/b&gt;

%1</source>
        <translation>&lt;b&gt;Следующий текст был скопирован в буфер обмена:&lt;/b&gt;

%1</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.cpp" line="76"/>
        <source>Qt Version</source>
        <translation>Версия Qt</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.cpp" line="83"/>
        <source>Operating System</source>
        <translation>Система</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.cpp" line="128"/>
        <source>Execuable Path</source>
        <translation>Выполняемый файл</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.cpp" line="141"/>
        <source>Loaded Modules</source>
        <translation>Модули системы</translation>
    </message>
</context>
<context>
    <name>CoreGUI::ConfirmCloseDialod</name>
    <message>
        <source>Confirm quit</source>
        <translation type="obsolete">Подвтерждение выхода</translation>
    </message>
    <message>
        <source>Do one of the following on quit:</source>
        <translation type="obsolete">Перед выходом из Кумир:</translation>
    </message>
    <message>
        <source>Do not save files, but save state to be restored on next launch</source>
        <translation type="obsolete">Сохранить состояние редактирования, но не сохранять файлы</translation>
    </message>
    <message>
        <source>Save all opened files and close them</source>
        <translation type="obsolete">Сохранить и закрыть все открытые файлы</translation>
    </message>
    <message>
        <source>Save unsaved file</source>
        <translation type="obsolete">Сохранить файл</translation>
    </message>
    <message>
        <source>Save nothing and quit</source>
        <translation type="obsolete">Выйти без сохранения</translation>
    </message>
    <message>
        <source>Cancel quit</source>
        <translation type="obsolete">Отменить выход</translation>
    </message>
</context>
<context>
    <name>CoreGUI::DebuggerView</name>
    <message>
        <location filename="../../../src/plugins/coregui/debuggerview.cpp" line="60"/>
        <source>Current values available only while running program in step-by-step mode</source>
        <translation>Текущие значения величин отображаются при выполнении по шагам</translation>
    </message>
</context>
<context>
    <name>CoreGUI::DebuggerWindow</name>
    <message>
        <source>Main algorithm</source>
        <translation type="obsolete">Главный алгоритм</translation>
    </message>
    <message>
        <source>Algorithm &apos;%1&apos;</source>
        <translation type="obsolete">Алгоритм &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Globals</source>
        <translation type="obsolete">Глобальные величины</translation>
    </message>
    <message>
        <source>Globals of &apos;%1&apos;</source>
        <translation type="obsolete">Глобальные в исполнителе &quot;%1&quot; </translation>
    </message>
    <message>
        <source>This variable is a reference.
Right click to navigate target</source>
        <translation type="obsolete">Эта величина является ссылкой. Для перехода по ссылке нажмите правую кнопку мыши</translation>
    </message>
    <message>
        <source>Current values available only while running program in step-by-step mode</source>
        <translation type="obsolete">Текущие значения величин отображаются при выполнении по шагам</translation>
    </message>
</context>
<context>
    <name>CoreGUI::GUISettingsPage</name>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.ui" line="14"/>
        <source>User Interface</source>
        <translation>Интерфейс</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.ui" line="20"/>
        <source>Docking layout</source>
        <translation>Расположение дополнительных окон</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.ui" line="41"/>
        <location filename="../../../src/plugins/coregui/guisettingspage.ui" line="48"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.ui" line="55"/>
        <source>Rows first</source>
        <translation>Исполнители 
в одной строке 
с окном ввода-вывода</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.ui" line="62"/>
        <source>Columns first</source>
        <translation>Исполнители 
в одном столбце 
с Практикумом</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.ui" line="85"/>
        <source>Visible icons in toolbar</source>
        <translation>Отображаемые значки над редактором</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="79"/>
        <source>Cut selection to clipboard</source>
        <translation>Вырезать</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="85"/>
        <source>Copy selection to clipboard</source>
        <translation>Скопировать</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="91"/>
        <source>Paste from clipboard</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="107"/>
        <source>Undo last action</source>
        <translation>Отменить последнее действие</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="113"/>
        <source>Redo last undoed action</source>
        <translation>Повторить последнее действие</translation>
    </message>
</context>
<context>
    <name>CoreGUI::KumirProgram</name>
    <message>
        <source>Fast run</source>
        <translation type="obsolete">Ускоренное выполнение</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="135"/>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="38"/>
        <source>Regular run</source>
        <translation>Обычное выполнение</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="51"/>
        <source>Testing run</source>
        <translation>Запустить тестирование</translation>
    </message>
    <message>
        <source>Step run</source>
        <translation type="obsolete">Пошаговое выполнение</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="163"/>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="67"/>
        <source>Step in</source>
        <translation>шаг</translation>
    </message>
    <message>
        <source>Step out</source>
        <translation type="obsolete">Выход из алгоритма</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="141"/>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="89"/>
        <source>Stop</source>
        <translation>Остановить выполнение</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="129"/>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="111"/>
        <source>Blind run</source>
        <translation>Без показа на полях</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="245"/>
        <source>This program does not have testing algorithm</source>
        <translation>У этой программы нет тестирующего алгоритма</translation>
    </message>
    <message>
        <source>This program does not have testing algorhitm</source>
        <translation type="obsolete">Программа не содержит тестирующего алгоритма</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="65"/>
        <source>Do big step</source>
        <translation>ШАГ</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="76"/>
        <source>Do small step</source>
        <translation>шаг</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="169"/>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="78"/>
        <source>Step to end</source>
        <translation>До конца алгоритма</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="87"/>
        <source>Run to end of algorhitm</source>
        <translation>До конца алгоритма</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="obsolete">Нераспознанная ошибка</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="379"/>
        <source>Evaluation error</source>
        <translation>Ошибка выполнения</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="388"/>
        <source>Evaluation finished</source>
        <translation>Выполнение завершено</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="370"/>
        <source>Evaluation terminated</source>
        <translation>Выполнение прервано</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="157"/>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="56"/>
        <source>Step over</source>
        <translation>ШАГ</translation>
    </message>
</context>
<context>
    <name>CoreGUI::KumirVariablesWebObject</name>
    <message>
        <source>User algorhitms and variables</source>
        <translation type="obsolete">Алгоритмы и величины</translation>
    </message>
    <message>
        <source>Global variables</source>
        <translation type="obsolete">Глобальные величины</translation>
    </message>
    <message>
        <source>Main algorhitm</source>
        <translation type="obsolete">Главный алгоритм</translation>
    </message>
    <message>
        <source>Algorhitm &quot;%1&quot;</source>
        <translation type="obsolete">Алгоритм «%1»</translation>
    </message>
    <message>
        <source>User algorithm and variables</source>
        <translation type="obsolete">Алгоритмы и величины</translation>
    </message>
    <message>
        <source>Main algorithm</source>
        <translation type="obsolete">Главный алгоритм</translation>
    </message>
    <message>
        <source>Algorithm &quot;%1&quot;</source>
        <translation type="obsolete">Алгоритм &quot;%1&quot;</translation>
    </message>
    <message>
        <source>integer</source>
        <translation type="obsolete">цел</translation>
    </message>
    <message>
        <source>real</source>
        <translation type="obsolete">вещ</translation>
    </message>
    <message>
        <source>charect</source>
        <translation type="obsolete">сим</translation>
    </message>
    <message>
        <source>string</source>
        <translation type="obsolete">лит</translation>
    </message>
    <message>
        <source>boolean</source>
        <translation type="obsolete">лог</translation>
    </message>
    <message>
        <source>table</source>
        <translation type="obsolete">таб</translation>
    </message>
    <message>
        <source>undefined</source>
        <translation type="obsolete">не определено</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Имя</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">Тип</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="obsolete">Значение</translation>
    </message>
    <message>
        <source>Global table &quot;%1&quot;</source>
        <translation type="obsolete">Глобальная таблица «%1»</translation>
    </message>
    <message>
        <source>Table &quot;%1&quot; from main algorhitm</source>
        <translation type="obsolete">Таблица «%1» главного алгоритма</translation>
    </message>
    <message>
        <source>Table &quot;%1&quot; from algorhitm &quot;%2&quot;</source>
        <translation type="obsolete">Таблица «%1» алгоритма «%2»</translation>
    </message>
    <message>
        <source> (module &quot;%1&quot;)</source>
        <translation type="obsolete"> (испонитель «%1»)</translation>
    </message>
    <message>
        <source>Unknown table</source>
        <translation type="obsolete">Неизвестная таблица</translation>
    </message>
    <message>
        <source>The table is not initialized yet</source>
        <translation type="obsolete">Таблица пока не определена</translation>
    </message>
    <message>
        <source>Yout must run program as regular or step run to show table values</source>
        <translation type="obsolete">Запустите программу в обычном или пошаговом режиме</translation>
    </message>
</context>
<context>
    <name>CoreGUI::KumirVariablesWindow</name>
    <message>
        <source>Name</source>
        <translation type="obsolete">Имя</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">Тип</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="obsolete">Значение</translation>
    </message>
    <message>
        <source>?</source>
        <translation type="obsolete">?</translation>
    </message>
</context>
<context>
    <name>CoreGUI::MainWindow</name>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="14"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="727"/>
        <source>Kumir</source>
        <translation>Кумир</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="56"/>
        <source>File</source>
        <translation>Программа</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="obsolete">Создать</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="82"/>
        <source>Help</source>
        <translation>Инфо</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="90"/>
        <source>Edit</source>
        <translation>Редактирование</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="95"/>
        <source>Insert</source>
        <translation>Вставка</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="100"/>
        <source>Run</source>
        <translation>Выполнение</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="105"/>
        <source>Window</source>
        <translation>Окна</translation>
    </message>
    <message>
        <source>Program</source>
        <translation type="obsolete">Программа</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="121"/>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="obsolete">Текст</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="118"/>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="51"/>
        <source>New program</source>
        <translation>Новая программа</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="126"/>
        <source>New text</source>
        <translation>Новый текст</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="129"/>
        <source>Ctrl+Shift+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="134"/>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="57"/>
        <source>Open...</source>
        <translation>Загрузить программу...</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="137"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="142"/>
        <source>Recent files</source>
        <translation>Недавние файлы</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="147"/>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="63"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1009"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1450"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1557"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1746"/>
        <source>Save</source>
        <translation>Сохранить программу</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="150"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="155"/>
        <source>Save as...</source>
        <translation>Сохранить программу как...</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="160"/>
        <source>Save all</source>
        <translation>Сохранить во всех вкладках</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="163"/>
        <source>Ctrl+Shift+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="168"/>
        <source>Close</source>
        <translation>Закрыть вкладку</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="171"/>
        <source>Ctrl+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="176"/>
        <source>Switch workspace...</source>
        <translation>Выбрать каталог пользователя...</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="181"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="184"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="195"/>
        <source>Manuals</source>
        <translation>Справочные руководства</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="235"/>
        <source>Variable Current Values</source>
        <translation>Текущие значения величин</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="249"/>
        <source>Show Console Pane</source>
        <translation>Отображать область ввода/вывода</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="252"/>
        <source>F12</source>
        <translation>F12</translation>
    </message>
    <message>
        <source>Variables</source>
        <translation type="obsolete">Величины</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="238"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <source>Usage...</source>
        <translation type="obsolete">Руководство пользователя...</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="198"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="206"/>
        <source>About...</source>
        <translation>О программе...</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="211"/>
        <source>Preferences...</source>
        <translation>Настройки...</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="219"/>
        <source>New Pascal program</source>
        <translation>Новая Паскаль-программа</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="222"/>
        <source>Ctrl+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="227"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1382"/>
        <source>Restore previous session</source>
        <translation>Восстановить предыдущий сеанс</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="107"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="114"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="121"/>
        <source>No actions for this tab</source>
        <translation>Для этой вкладки нет действий</translation>
    </message>
    <message>
        <source>No errors</source>
        <translation type="obsolete">Ошибок нет</translation>
    </message>
    <message>
        <source>Errors: %1</source>
        <translation type="obsolete">Ошибок: %1</translation>
    </message>
    <message>
        <source>Kumir programs (*.kum)</source>
        <translation type="obsolete">Программы Кумир (*.kum)</translation>
    </message>
    <message>
        <source>Pascal programs (*.pas *.pp</source>
        <translation type="obsolete">Программы Паскаль (*.pas *.pp)</translation>
    </message>
    <message>
        <source>Steps done: %1</source>
        <translation type="obsolete">Выполнено шагов: %1</translation>
    </message>
    <message>
        <source>New Program</source>
        <translation type="obsolete">Новая программа</translation>
    </message>
    <message>
        <source>New Text</source>
        <translation type="obsolete">Новый текст</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1586"/>
        <source>%1 programs (*%2)</source>
        <translation>Программы %1 (*%2)</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="928"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1589"/>
        <source>Text files (*.txt)</source>
        <translation>Текстовые файлы (*.txt)</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="925"/>
        <source>%1 programs (*.%2)</source>
        <translation>Программы %1 (*.%2)</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="930"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1591"/>
        <source>All files (*)</source>
        <translation>Все файлы (*)</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="931"/>
        <source>Save file</source>
        <translation>Сохранить файл</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="958"/>
        <source>Can&apos;t save file</source>
        <translation>Не могу сохранить файл</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1003"/>
        <source>Close editor</source>
        <translation>Закрытие текста</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1004"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1552"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1741"/>
        <source>Save current text?</source>
        <translation>Сохранить текущий текст?</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1011"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1452"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1559"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1748"/>
        <source>Don&apos;t save</source>
        <translation>Не сохранять</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1013"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1454"/>
        <source>Cancel closing</source>
        <translation>Отменить закрытие</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1056"/>
        <source>Preferences</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1383"/>
        <source>Are you sure to restore previous session? All unsaved changes will be lost.</source>
        <translation>Вы уверены, что хотите восстановить предыдущий сеанс? Все открытые файлы будут закрыты без сохранения.</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1387"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1388"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1441"/>
        <source>The following files have changes:
%1
Save them?</source>
        <translation>Эти файлы были изменены:
%1
Сохранить их?</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1444"/>
        <source>Close Kumir</source>
        <translation>Выход из Кумир</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1551"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1740"/>
        <source>Open another file</source>
        <translation>Открытие другого файла</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1561"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1750"/>
        <source>Cancel opening another file</source>
        <translation>Не открывать другой файл</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1588"/>
        <source>Web pages (*.html *.htm)</source>
        <translation>Web-страницы (*.html *.htm)</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1809"/>
        <source>Can&apos;t open file</source>
        <translation>Не могу открыть файл</translation>
    </message>
    <message>
        <source>Before creating new program:</source>
        <translation type="obsolete">Перед созданием новой программы:</translation>
    </message>
    <message>
        <source>Before closing tab:</source>
        <translation type="obsolete">Перед закрытием вкладки:</translation>
    </message>
    <message>
        <source>Cancel &quot;New program&quot;</source>
        <translation type="obsolete">Отменить создание новой программы</translation>
    </message>
    <message>
        <source>Cancel tab close</source>
        <translation type="obsolete">Отменить закрытие вкладки</translation>
    </message>
    <message>
        <source>Close without saving</source>
        <translation type="obsolete">Закрыть без сохранения</translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="obsolete">Начало</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="obsolete">О программе</translation>
    </message>
    <message>
        <source>Kumir version %1
Using Qt version %2</source>
        <translation type="obsolete">Кумир версия %1
Использует Qt версии %2</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1595"/>
        <source>Load file...</source>
        <translation>Загрузить файл...</translation>
    </message>
    <message>
        <source>Kumir programs (*.kum);;Pascal programs (*.pas *.pp);;Web pages (*.html *.htm);;Text files (*.txt);;All files (*)</source>
        <translation type="obsolete">Программы Кумир (*.kum);;Программы Pascal (*.pas *.pp);; Web-страницы (*.html *.htm);;Текстовые файлы (*.txt);;Все файлы (*)</translation>
    </message>
</context>
<context>
    <name>CoreGUI::PascalProgram</name>
    <message>
        <source>Stop</source>
        <translation type="obsolete">Остановить выполнение</translation>
    </message>
    <message>
        <source>Blind run</source>
        <translation type="obsolete">Без показа на полях и обновления окна величин</translation>
    </message>
</context>
<context>
    <name>CoreGUI::Plugin</name>
    <message>
        <source>Input/Output terminal</source>
        <translation type="obsolete">Область ввода/вывода</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="413"/>
        <source>Variables</source>
        <translation>Значения величин</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="347"/>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="358"/>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="216"/>
        <source>Remote Control</source>
        <translation>Пульт</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="67"/>
        <source>PROGRAM.kum</source>
        <translation>ПРОГРАММА.kum</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="68"/>
        <source>Source file name</source>
        <translation>Имя файла</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="139"/>
        <source>Input/Output</source>
        <translation>Ввод/вывод</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="165"/>
        <source>Save console output</source>
        <translation>Сохранить вывод</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="176"/>
        <source>Copy to clipboard console output</source>
        <translation>Скопировать вывод в буфер обмена</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="230"/>
        <source>Help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="253"/>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="263"/>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="185"/>
        <source>Courses</source>
        <translation>Практикум</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="385"/>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="386"/>
        <source>No actions for this tab</source>
        <translation>Для этой вкладки нет действий</translation>
    </message>
    <message>
        <source>Editing</source>
        <translation type="obsolete">Редактирование</translation>
    </message>
    <message>
        <source>Observe</source>
        <translation type="obsolete">Анализ</translation>
    </message>
    <message>
        <source>Running</source>
        <translation type="obsolete">Выполнение</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="obsolete">Пауза</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="390"/>
        <source>Start</source>
        <translation>Начало</translation>
    </message>
</context>
<context>
    <name>CoreGUI::StatusBar</name>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="144"/>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="489"/>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="493"/>
        <source>rus</source>
        <translation>рус</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="145"/>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="487"/>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="495"/>
        <source>lat</source>
        <translation>lat</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="160"/>
        <source>Row: ww, Col.: ww</source>
        <translation>Стр: ww, Кол: ww</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="169"/>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="327"/>
        <source>Edit</source>
        <translation>Редактирование</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="169"/>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="320"/>
        <source>Analisys</source>
        <translation>Анализ</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="169"/>
        <source>Run</source>
        <translation>Выполнение</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="169"/>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="317"/>
        <source>Pause</source>
        <translation>Пауза</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="184"/>
        <source>ww errors</source>
        <translation>ww ошибок</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="185"/>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="356"/>
        <source>No errors</source>
        <translation>Ошибок нет</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="186"/>
        <source>wwwww steps done</source>
        <translation>Выполнено wwwww шагов</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="323"/>
        <source>Running</source>
        <translation>Выполнение</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="358"/>
        <source>1 error</source>
        <translation>1 ошибка</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="360"/>
        <source>%1 errors</source>
        <comment>10 &lt;= x &lt;= 20</comment>
        <translation>%1 ошибок</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="362"/>
        <source>%1 errors</source>
        <comment>1, 21, 31, etc.</comment>
        <translation>%1 ошибка</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="364"/>
        <source>%1 errors</source>
        <comment>2, 3, 4, 22, 23, 24,  etc.</comment>
        <translation>%1 ошибки</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="366"/>
        <source>%1 errors</source>
        <comment>5, 6, 15, 16, etc</comment>
        <translation>%1 ошибок</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="374"/>
        <source>%1 steps done</source>
        <comment>10 &lt;= x &lt;= 20</comment>
        <translation>Выполнено шагов: %1</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="376"/>
        <source>%1 steps done</source>
        <comment>1, 21, 31, etc.</comment>
        <translation>Выполнено шагов: %1</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="378"/>
        <source>%1 steps done</source>
        <comment>2, 3, 4, 22, 23, 24,  etc.</comment>
        <translation>Выполнено шагов: %1</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="380"/>
        <source>%1 steps done</source>
        <comment>5, 6, 15, 16, etc</comment>
        <translation>Выполнено шагов: %1</translation>
    </message>
    <message>
        <source>%1 errors</source>
        <translation type="obsolete">%1 ошибок</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="370"/>
        <source>0 steps done</source>
        <translation>Выполнено шагов: 1</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="372"/>
        <source>1 step done</source>
        <translation>Выполнено шагов: 1</translation>
    </message>
    <message>
        <source>%1 steps done</source>
        <translation type="obsolete">Выполнено %1 шагов</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="455"/>
        <source>Row: %1, Column: %2</source>
        <translation>Стр: %1, Кол: %2</translation>
    </message>
</context>
<context>
    <name>CoreGUI::SwitchWorkspaceDialog</name>
    <message>
        <location filename="../../../src/plugins/coregui/switchworkspacedialog.ui" line="20"/>
        <source>Workspace Launcher</source>
        <translation>Выбор каталога пользователя</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/switchworkspacedialog.ui" line="44"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Droid Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Select a workspace&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Kumir stores your programs, data and settings in a folder called a workspace.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choose a workspace folder to use for this session.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Droid Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Выберете рабочий каталог&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Кумир хранит Ваши программы, настройки и файлы данных в едином рабочем каталоге.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Выберете каталог, который будет использовать Кумир.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/switchworkspacedialog.ui" line="60"/>
        <source>Workspace:</source>
        <translation>Каталог пользователя:</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/switchworkspacedialog.ui" line="80"/>
        <source>Browse...</source>
        <translation>Обзор...</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/switchworkspacedialog.ui" line="89"/>
        <source>Use this as the default and do not ask again</source>
        <translation>Использовать этот каталог всегда и больше не спрашивать</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/switchworkspacedialog.cpp" line="53"/>
        <source>Select directory for use as workspace</source>
        <translation>Выбор рабочего каталога</translation>
    </message>
</context>
<context>
    <name>CoreGUI::SystemOpenFileSettings</name>
    <message>
        <location filename="../../../src/plugins/coregui/systemopenfilesettings.ui" line="14"/>
        <source>File Open</source>
        <translation>Открытие файлов</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/systemopenfilesettings.ui" line="20"/>
        <source>Open .kum file by system in:</source>
        <translation>Открывать файлы &quot;*.kum&quot; в:</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/systemopenfilesettings.cpp" line="26"/>
        <source>Choose on run</source>
        <translation>Спросить при запуске</translation>
    </message>
</context>
<context>
    <name>CoreGUI::TabBar</name>
    <message>
        <location filename="../../../src/plugins/coregui/tabbar.cpp" line="129"/>
        <location filename="../../../src/plugins/coregui/tabbar.cpp" line="131"/>
        <source>&lt;b&gt;Ctrl+%1&lt;/b&gt; activates this tab</source>
        <translation>&lt;b&gt;Ctrl+%1&lt;/b&gt; переключает на эту вкладку</translation>
    </message>
</context>
<context>
    <name>CoreGUI::TabWidget</name>
    <message>
        <source>Close current tab</source>
        <translation type="obsolete">Закрыть текущую вкладку</translation>
    </message>
</context>
<context>
    <name>CoreGUI::TabWidgetElement</name>
    <message>
        <location filename="../../../src/plugins/coregui/tabwidgetelement.cpp" line="223"/>
        <source>%1 (Course)</source>
        <translation>%1 (Практикум)</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/tabwidgetelement.cpp" line="228"/>
        <source>New Program</source>
        <translation>Новая программа</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/tabwidgetelement.cpp" line="231"/>
        <source>New Text</source>
        <translation>Новый текст</translation>
    </message>
</context>
<context>
    <name>CoreGUI::ToolbarContextMenu</name>
    <message>
        <location filename="../../../src/plugins/coregui/toolbarcontextmenu.cpp" line="30"/>
        <source>Customize tool bar icons</source>
        <translation>Настроить отображаемые значки</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/toolbarcontextmenu.cpp" line="80"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/toolbarcontextmenu.cpp" line="81"/>
        <source>Reset to default</source>
        <translation>Сброк настроек отображения</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/toolbarcontextmenu.cpp" line="82"/>
        <source>Check all</source>
        <translation>Показывать все</translation>
    </message>
</context>
<context>
    <name>Terminal::OneSession</name>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="690"/>
        <source>&gt;&gt; %1:%2:%3 - %4 - Process started</source>
        <translation>&gt;&gt; %1:%2:%3 - %4 - Начало выполнения</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="700"/>
        <source>&gt;&gt; %1:%2:%3 - %4 - Process finished</source>
        <translation>&gt;&gt; %1:%2:%3 - %4 - Выполнение завершено</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="756"/>
        <source>INPUT raw data to console stream</source>
        <translation>Ввод данных в файл &quot;консоль&quot;</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="759"/>
        <source>INPUT </source>
        <translation>ВВОД </translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="766"/>
        <source>string</source>
        <translation>лит</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="768"/>
        <source>integer</source>
        <translation>цел</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="770"/>
        <source>real</source>
        <translation>вещ</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="772"/>
        <source>charect</source>
        <translation>сим</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="774"/>
        <source>boolean</source>
        <translation>лог</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="937"/>
        <source>INPUT ERROR: Not a &apos;%1&apos; value</source>
        <translation>ОШИБКА ВВОДА: Это не значение &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="946"/>
        <source>INPUT ERROR: Extra input</source>
        <translation>ОШИБКА ВВОДА: Введено лишнее</translation>
    </message>
    <message>
        <source>Not a &apos;%1&apos; value</source>
        <translation type="obsolete">Это не значение типа %1</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="990"/>
        <source>RUNTIME ERROR: %1</source>
        <translation>ОШИБКА ВЫПОЛНЕНИЯ: %1</translation>
    </message>
</context>
<context>
    <name>Terminal::Plane</name>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_plane.cpp" line="22"/>
        <source>Copy to clipboard</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_plane.cpp" line="26"/>
        <source>Paste from clipboard</source>
        <translation>Вставить</translation>
    </message>
</context>
<context>
    <name>Terminal::Term</name>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="13"/>
        <source>Input/Output</source>
        <translation>Ввод/вывод</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="51"/>
        <source>Save last output</source>
        <translation>Сохранить последний вывод</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="55"/>
        <source>Copy last output</source>
        <translation>Скопировать последний вывод</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="59"/>
        <source>Copy all output</source>
        <translation>Скопировать весь вывод</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="63"/>
        <source>Open last output in editor</source>
        <translation>Открыть последний вывод в редакторе</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="68"/>
        <source>Save all output</source>
        <translation>Сохранить весь вывод</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="72"/>
        <source>Clear output</source>
        <translation>Очистить область ввода/вывода</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="249"/>
        <source>New Program</source>
        <translation>Новая программа</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="408"/>
        <source>Save output...</source>
        <translation>Сохранить вывод в файл...</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="410"/>
        <source>Text files (*.txt);;All files (*)</source>
        <translation>Текстовые файлы (*.txt);;Все файлы (*)</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="422"/>
        <source>Can&apos;t save output</source>
        <translation>Невозможно сохранить вывод</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="423"/>
        <source>The file you selected can not be written</source>
        <translation>Выбранный Вами файл не может быть создан или перезаписан</translation>
    </message>
</context>
<context>
    <name>Terminal::Terminal</name>
    <message>
        <source>Save last output</source>
        <translation type="obsolete">Сохранить последний вывод</translation>
    </message>
    <message>
        <source>Open last output in editor</source>
        <translation type="obsolete">Открыть последний вывод в редакторе</translation>
    </message>
    <message>
        <source>Save all output</source>
        <translation type="obsolete">Сохранить весь вывод</translation>
    </message>
    <message>
        <source>Clear output</source>
        <translation type="obsolete">Очистить область ввода/вывода</translation>
    </message>
</context>
</TS>

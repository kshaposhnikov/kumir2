<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>DocBookViewer::ContentView</name>
    <message>
        <location filename="../../../src/shared/docbookviewer/contentview.cpp" line="511"/>
        <location filename="../../../src/shared/docbookviewer/contentview.cpp" line="524"/>
        <location filename="../../../src/shared/docbookviewer/contentview.cpp" line="1403"/>
        <source>Table&amp;nbsp;%1. </source>
        <translation>Таблица&amp;nbsp;%1. </translation>
    </message>
    <message>
        <source>Context:</source>
        <translation type="obsolete">Контекст:</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/contentview.cpp" line="667"/>
        <source>See %1 for more details.</source>
        <translation>Подробное описание см. в %1</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/contentview.cpp" line="687"/>
        <location filename="../../../src/shared/docbookviewer/contentview.cpp" line="706"/>
        <location filename="../../../src/shared/docbookviewer/contentview.cpp" line="1402"/>
        <source>Example&amp;nbsp;%1. </source>
        <translation>Пример&amp;nbsp;%1. </translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/contentview.cpp" line="727"/>
        <source>Algorithm </source>
        <translation>Алгоритм</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/contentview.cpp" line="758"/>
        <source>Synopsis:</source>
        <translation>Синтаксис:</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/contentview.cpp" line="992"/>
        <source>Figure&amp;nbsp;%1 </source>
        <translation>Рисунок&amp;nbsp;%1. </translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/contentview.cpp" line="1227"/>
        <source>(see&amp;nbsp;&lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;)</source>
        <translation>(см.&amp;nbsp;&lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;)</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/contentview.cpp" line="1334"/>
        <source>List of examples in &quot;%1&quot;</source>
        <translation>Список примеров в &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/contentview.cpp" line="1337"/>
        <source>List of tables in &quot;%1&quot;</source>
        <translation>Список таблиц в &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/contentview.cpp" line="1341"/>
        <source>List of Standard Library algorithms</source>
        <translation>Список встроенных алгоритмов</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/contentview.cpp" line="1343"/>
        <source>List of algorithms of module &quot;%1&quot;</source>
        <translation>Список алгоритмов исполнителя &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>DocBookViewer::DocBookViewImpl</name>
    <message>
        <location filename="../../../src/shared/docbookviewer/docbookview_impl.cpp" line="80"/>
        <source>Hide side bar</source>
        <translation>Скрыть панель навигации</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/docbookview_impl.cpp" line="81"/>
        <source>Show side bar</source>
        <translation>Показать панель навигации</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/docbookview_impl.cpp" line="219"/>
        <source>Toggle sidebar visible</source>
        <translation>Показывать боковую панель</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/docbookview_impl.cpp" line="222"/>
        <source>Print...</source>
        <translation>Печать...</translation>
    </message>
</context>
<context>
    <name>DocBookViewer::MainWindow</name>
    <message>
        <location filename="../../../src/shared/docbookviewer/mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/mainwindow.ui" line="34"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/mainwindow.ui" line="46"/>
        <source>&amp;Open...</source>
        <translation>Открыть...</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/mainwindow.ui" line="49"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/mainwindow.ui" line="54"/>
        <source>E&amp;xit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/mainwindow.ui" line="57"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/mainwindow.ui" line="62"/>
        <source>&amp;Print...</source>
        <translation>Печать...</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/mainwindow.ui" line="65"/>
        <source>Ctrl+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/mainwindow.cpp" line="31"/>
        <source>Open DocBook document</source>
        <translation>Открыть документ DocBook</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/mainwindow.cpp" line="33"/>
        <source>DocBook XML (*.xml);;All files (*)</source>
        <translation>Файлы XML (*.xml);;Все файлы (*)</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/mainwindow.cpp" line="42"/>
        <source>Can&apos;t open file</source>
        <translation>Не могу открыть файл</translation>
    </message>
</context>
<context>
    <name>DocBookViewer::PrintDialog</name>
    <message>
        <location filename="../../../src/shared/docbookviewer/printdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/printdialog.ui" line="20"/>
        <source>Items to print</source>
        <translation>Что напечатать</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/printdialog.ui" line="41"/>
        <source>Pages options</source>
        <translation>Оценка количества страниц</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/printdialog.ui" line="47"/>
        <source>Page size:</source>
        <translation>Формат страницы:</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/printdialog.ui" line="55"/>
        <source>A4</source>
        <translation>А4</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/printdialog.ui" line="60"/>
        <source>A5</source>
        <translation>А5</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/printdialog.ui" line="68"/>
        <source>Pages count: unknown</source>
        <translation>Число страниц: неизвестно</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/printdialog.ui" line="78"/>
        <source>Estimate pages count</source>
        <translation>Оценить</translation>
    </message>
</context>
<context>
    <name>DocBookViewer::SidePanel</name>
    <message>
        <location filename="../../../src/shared/docbookviewer/sidepanel.ui" line="26"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/sidepanel.ui" line="82"/>
        <source>Contents</source>
        <translation>Содержание</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/sidepanel.ui" line="100"/>
        <source>Algorithms</source>
        <translation>Алгоритмы</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/sidepanel.ui" line="138"/>
        <location filename="../../../src/shared/docbookviewer/sidepanel.ui" line="180"/>
        <location filename="../../../src/shared/docbookviewer/sidepanel.ui" line="222"/>
        <location filename="../../../src/shared/docbookviewer/sidepanel.ui" line="264"/>
        <source>Filter:</source>
        <translation>&amp;Фильтр:</translation>
    </message>
    <message>
        <source>Index</source>
        <translation type="obsolete">Поиск</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/sidepanel.ui" line="52"/>
        <source>Examples</source>
        <translation>Примеры</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/sidepanel.ui" line="67"/>
        <source>Tables</source>
        <translation>Таблицы</translation>
    </message>
    <message>
        <source>Search:</source>
        <translation type="obsolete">Искать:</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/sidepanel.cpp" line="231"/>
        <source>List of examples in &quot;%1&quot;</source>
        <translation>Список примеров в &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/sidepanel.cpp" line="259"/>
        <source>List of tables in &quot;%1&quot;</source>
        <translation>Список таблиц в &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../../src/shared/docbookviewer/sidepanel.cpp" line="279"/>
        <source>Standard Library functions</source>
        <translation>Встроенные алгоритмы</translation>
    </message>
</context>
</TS>

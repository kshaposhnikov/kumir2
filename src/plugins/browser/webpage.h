#ifndef BROWSER_WEBPAGE_H
#define BROWSER_WEBPAGE_H

#include <QtWebKit/QWebSettings>
#if QT_VERSION >= 0x050000
	#include <QtWebKitWidgets/QWebView>
	#include <QtWebKitWidgets/QWebPage>
	#include <QtWebKitWidgets/QWebFrame>
#else
	#include <QtWebKit/QWebView>
	#include <QtWebKit/QWebPage>
	#include <QtWebKit/QWebFrame>
#endif

namespace Browser {

class WebPage : public QWebPage
{
    Q_OBJECT
public:
    WebPage();

    void setComponent(class Component * v);

protected:
    QObject * createPlugin(const QString &classid, const QUrl &url, const QStringList &paramNames, const QStringList &paramValues);

private:
    class Component * component_;


};

} // namespace Browser

#endif // BROWSER_WEBPAGE_H
